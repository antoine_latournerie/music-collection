/* eslint-disable import/no-extraneous-dependencies */
import { createStore, Store } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import reducer from './reducer'
import StoreState from './state'

const store: Store<StoreState> = createStore(reducer, composeWithDevTools())

export default store
