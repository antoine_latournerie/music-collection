import StoreState from '../state'
import CollectionState from './state'

export function getCollection(state: StoreState): CollectionState {
  return state.collection
}

export function isAlbumInCollection(state: StoreState, albumId: string): boolean {
  return typeof getCollection(state).find(id => id === albumId) !== 'undefined'
}
