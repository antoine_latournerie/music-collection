import { Action } from 'redux'

export enum CollectionActionTypes {
  ADD = '[Collection] ADD',
  REMOVE = '[Collection] REMOVE',
}

interface AddToCollectionAction extends Action<CollectionActionTypes.ADD> {
  id: string,
}

interface RemoveFromCollectionAction extends Action<CollectionActionTypes.REMOVE> {
  id: string,
}

export function addToCollection(id: string): AddToCollectionAction {
  return {
    type: CollectionActionTypes.ADD,
    id,
  }
}

export function removeFromCollection(id: string): RemoveFromCollectionAction {
  return {
    type: CollectionActionTypes.REMOVE,
    id,
  }
}

export type CollectionAction = AddToCollectionAction | RemoveFromCollectionAction
