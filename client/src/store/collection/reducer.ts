import { Reducer } from 'redux'

import { CollectionAction, CollectionActionTypes } from './actions'
import CollectionState from './state'

const initialState: CollectionState = []

const collection: Reducer<CollectionState, CollectionAction> = (state = initialState, action) => {
  switch (action.type) {
    case CollectionActionTypes.ADD: {
      return state.concat(action.id)
    }
    case CollectionActionTypes.REMOVE: {
      return state.filter(id => id !== action.id)
    }
    default: {
      return state
    }
  }
}

export default collection
