import CollectionState from './collection/state'

export default interface StoreState {
  collection: CollectionState,
}
