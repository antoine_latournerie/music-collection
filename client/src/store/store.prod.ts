import { createStore, Store } from 'redux'

import reducer from './reducer'
import StoreState from './state'

const store: Store<StoreState> = createStore(reducer)

export default store
