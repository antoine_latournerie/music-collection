import { combineReducers } from 'redux'
import collection from './collection/reducer'

export default combineReducers({
  collection,
})
