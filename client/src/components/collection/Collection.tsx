import React from 'react'
import styled from 'styled-components'

import AlbumGrid from '../search/AlbumGrid'

const Title = styled.h1`
  margin: 0.6em 5.7%;
`

interface Props {
  collection: SpotifyApi.AlbumObjectFull[]
}

const Collection: React.FunctionComponent<Props> = ({ collection }) => (
  <div>
    <Title>Collection</Title>
    {collection && collection.length > 0
      ? (
        <AlbumGrid
          albums={collection}
        />
      )
      : <span>No album in collection</span>
    }
  </div>
)

export default Collection
