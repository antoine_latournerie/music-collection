import React from 'react'
import { connect } from 'react-redux'

import { getCollection } from '../../store/collection/selectors'
import CollectionState from '../../store/collection/state'
import StoreState from '../../store/state'
import { getAlbums } from '../../utils/api'
import ErrorMessage from '../ui/ErrorMessage'
import Loading from '../ui/Loading'
import Collection from './Collection'

interface Props {
  collection: CollectionState
}

interface State {
  collection: SpotifyApi.AlbumObjectFull[]
  error: boolean
  loading: boolean
}

class CollectionContainer extends React.Component<Props, State> {
  readonly state: Readonly<State> = {
    collection: [],
    error: false,
    loading: false,
  }

  componentDidMount() {
    const { collection } = this.props
    this.fetchCollection(collection)
  }

  componentDidUpdate(prevProps: Props) {
    const { collection } = this.props
    if (collection !== prevProps.collection) {
      this.fetchCollection(collection)
    }
  }

  private async fetchCollection(collection: CollectionState) {
    if (collection.length === 0) {
      this.setState({ collection: [], loading: false, error: false })
      return
    }
    this.setState({ collection: [], loading: true, error: false })
    try {
      const result = await getAlbums(collection)
      this.setState({ collection: result, loading: false })
    } catch (e) {
      this.setState({ loading: false, error: true })
    }
  }

  render() {
    const { collection, loading, error } = this.state
    if (loading) {
      return <Loading />
    }
    if (error) {
      return <ErrorMessage />
    }
    return collection && <Collection collection={collection} />
  }
}

const mapStateToProps = (state: StoreState) => ({
  collection: getCollection(state),
})

export default connect(mapStateToProps)(CollectionContainer)
