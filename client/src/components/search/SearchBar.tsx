import React from 'react'
import { IoIosSearch } from 'react-icons/io'
import styled from 'styled-components'

import Button from '../ui/Button'

const StyledSearchBar = styled.div`
  margin: 1em;
`

const SearchForm = styled.form`
  margin: auto;
  position: relative;
  width: 80%;
`

const SearchInput = styled.input`
  background: none;
  border: 0;
  border-bottom: 2px solid #d5d5d5;
  box-sizing: border-box;
  color: #696969;
  font-size: 1.13em;
  margin: 0.33em 0;
  padding: 0.78em 2.38em 0.78em 0.33em;
  transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
  width: 100%;

  &:focus {
    border-color: #5dbad5;
    color: #555;
    outline: none;
  }
`

const SubmitButton = styled(Button)`
  padding: 0.65em;
  position: absolute;
  right: 0;
  top: 0.65em;
`

interface Props {
  searchTerm: string
  handleTermChange: (e: React.FormEvent<HTMLInputElement>) => void
  handleSubmit: (e: React.FormEvent<HTMLFormElement>) => void
}

const SearchBar: React.FunctionComponent<Props> = ({
  searchTerm,
  handleTermChange,
  handleSubmit,
}) => (
  <StyledSearchBar>
    <SearchForm autoComplete="off" autoCorrect="off" spellCheck={false} onSubmit={handleSubmit}>
      <SearchInput
        type="text"
        name="term"
        value={searchTerm}
        placeholder="Search"
        onChange={handleTermChange}
      />
      <SubmitButton type="submit">
        <IoIosSearch size="1.35em" color="#696969" />
      </SubmitButton>
    </SearchForm>
  </StyledSearchBar>
)

export default SearchBar
