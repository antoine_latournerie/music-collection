import React from 'react'
import loadable from 'loadable-components'
import styled from 'styled-components'

import ComponentLoading from '../ui/ComponentLoading'

const AlbumCard = loadable(
  () => import('./AlbumCard'),
  { LoadingComponent: ComponentLoading },
)

interface Props {
  albums: SpotifyApi.AlbumObjectSimplified[],
}

const Grid = styled.div`
  align-content: baseline;
  align-items: baseline;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  margin: 0 3.7%;
`

const AlbumGrid: React.FunctionComponent<Props> = ({ albums }) => (
  <Grid>
    {albums.map(album => (
      <AlbumCard
        key={album.id}
        album={album}
      />
    ))}
  </Grid>
)

export default AlbumGrid
