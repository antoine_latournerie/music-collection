import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { getImageUrl } from '../../utils/format'

const AlbumLink = styled(Link)`
  box-sizing: border-box;
  color: #000;
  margin-bottom: 0.6em;
  padding: 0 2%;
  text-decoration: none;
  width: 50%;

  @media only screen and (min-width: 750px) {
    padding: 0.5em;
    width: 25%;
  }

  &:hover {
    text-decoration: underline;
  }

  &:hover h4 {
    text-decoration: underline;
  }
`

const CoverWrapper = styled.div`
  box-sizing: border-box;
  display: flex;
  height: 100%;
  margin: auto;
  padding: 0;
  position: relative;
  width: 100%;

  @media only screen and (min-width: 750px) {
    height: 9.5em;
    padding: 0;
    width: 9.5em;
  }
`

const Cover = styled.img`
  align-self: flex-end;
  border: 1px solid rgba(13, 124, 141, 0.05);
  width: 100%;
`

const AlbumInformation = styled.div`
  display: block;
  margin-top: 0.6em;
  text-align: center;

  h4,
  h3 {
    margin: 0.2em 0;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`

interface Props {
  album: SpotifyApi.AlbumObjectSimplified
}

const AlbumCard: React.FunctionComponent<Props> = ({ album }) => {
  const artworkUrl = getImageUrl(album.images, 500)
  const artists = album.artists.map(artist => artist.name).join(', ')
  return (
    <AlbumLink to={`/album/${album.id}`}>
      <div>
        <CoverWrapper>
          <Cover src={artworkUrl} alt="artwork" />
        </CoverWrapper>
      </div>
      <AlbumInformation>
        <h3>{album.name}</h3>
        <h4>{artists}</h4>
      </AlbumInformation>
    </AlbumLink>
  )
}

export default AlbumCard
