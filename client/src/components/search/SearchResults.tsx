import React from 'react'
import loadable from 'loadable-components'

import ComponentLoading from '../ui/ComponentLoading'
import AlbumGrid from './AlbumGrid'

const AlbumCard = loadable(
  () => import('./AlbumCard'),
  { LoadingComponent: ComponentLoading },
)

interface Props {
  albums: SpotifyApi.PagingObject<SpotifyApi.AlbumObjectSimplified> | null
}

const SearchResults: React.FunctionComponent<Props> = ({ albums }) => {
  if (albums && albums.items && albums.items.length > 0) {
    return (
      <AlbumGrid albums={albums.items}>
        {albums.items.map(album => (
          <AlbumCard
            key={album.id}
            album={album}
          />
        ))}
      </AlbumGrid>
    )
  }
  return <span>No result found</span>
}

export default SearchResults
