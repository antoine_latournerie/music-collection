import React from 'react'
import loadable from 'loadable-components'

import ComponentLoading from '../ui/ComponentLoading'
import ErrorMessage from '../ui/ErrorMessage'
import Loading from '../ui/Loading'
import SearchBar from './SearchBar'

const SearchResults = loadable(
  () => import('./SearchResults'),
  { LoadingComponent: ComponentLoading },
)

interface Props {
  searchTerm: string
  urlSearchTerm: string
  albums: SpotifyApi.PagingObject<SpotifyApi.AlbumObjectSimplified> | null
  error: boolean
  loading: boolean
  handleTermChange: (e: React.FormEvent<HTMLInputElement>) => void
  handleSubmit: (e: React.FormEvent<HTMLFormElement>) => void
}

const Search: React.FunctionComponent<Props> = ({
  urlSearchTerm,
  searchTerm,
  albums,
  loading,
  error,
  handleTermChange,
  handleSubmit,
}) => (
  <div>
    <SearchBar
      searchTerm={searchTerm}
      handleTermChange={handleTermChange}
      handleSubmit={handleSubmit}
    />
    {loading && <Loading />}
    {!loading && (error
      ? <ErrorMessage />
      : urlSearchTerm && (
        <SearchResults albums={albums} />
      )
    )}
  </div>
)

export default Search
