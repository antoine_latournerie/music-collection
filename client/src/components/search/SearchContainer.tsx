import queryString from 'query-string'
import React from 'react'
import { RouteComponentProps, withRouter } from 'react-router'

import { searchAlbum } from '../../utils/api'
import Search from './Search'

interface State {
  searchTerm: string
  urlSearchTerm: string
  albums: SpotifyApi.PagingObject<SpotifyApi.AlbumObjectSimplified> | null
  error: boolean
  loading: boolean
}

type Props = {} & RouteComponentProps<{}>

class SearchContainer extends React.Component<Props, State> {
  readonly state: Readonly<State> = {
    searchTerm: '',
    urlSearchTerm: '',
    albums: null,
    error: false,
    loading: false,
  }

  static getDerivedStateFromProps(nextProps: Props, prevState: State) {
    const { q } = queryString.parse(nextProps.location.search)
    const urlSearchTerm = q || ''
    if (urlSearchTerm !== prevState.urlSearchTerm) {
      return {
        searchTerm: urlSearchTerm,
        urlSearchTerm,
      }
    }
    return null
  }

  componentDidMount() {
    const { searchTerm } = this.state
    if (searchTerm) {
      this.startSearch(searchTerm)
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    const { urlSearchTerm } = this.state
    if (urlSearchTerm !== prevState.urlSearchTerm) {
      this.startSearch(urlSearchTerm)
    }
  }

  private handleTermChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { value: searchTerm } = event.currentTarget
    this.setState({ searchTerm })
  }

  private handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const { searchTerm, urlSearchTerm } = this.state
    if (urlSearchTerm === searchTerm) {
      this.startSearch(searchTerm)
    } else {
      this.pushHistoryWithSearchTerm(searchTerm)
    }
  }

  private async startSearch(term: string): Promise<void> {
    if (term === '') {
      this.setState({ albums: null, loading: false, error: false })
      return
    }
    this.setState({ albums: null, loading: true, error: false })
    try {
      const { albums } = await searchAlbum({ q: term })
      this.setState({ albums, loading: false })
    } catch (e) {
      this.setState({ loading: false, error: true })
    }
  }

  private pushHistoryWithSearchTerm(searchTerm: string): void {
    const { history } = this.props
    history.push({
      pathname: '',
      search: `?q=${encodeURIComponent(searchTerm)}`,
    })
  }

  render() {
    const {
      urlSearchTerm,
      searchTerm,
      albums,
      loading,
      error,
    } = this.state

    return (
      <Search
        searchTerm={searchTerm}
        handleTermChange={this.handleTermChange}
        handleSubmit={this.handleSubmit}
        loading={loading}
        error={error}
        urlSearchTerm={urlSearchTerm}
        albums={albums}
      />
    )
  }
}

export default withRouter(SearchContainer)
