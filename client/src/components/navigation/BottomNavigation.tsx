import * as React from 'react'
import { IoIosSearch } from 'react-icons/io'
import { MdLibraryMusic, MdPersonOutline } from 'react-icons/md'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

const NavigationWrapper = styled.div`
  height: 45px;
`

const Navigation = styled.nav`
  align-items: stretch;
  background-color: #f4f5f7;
  border-top: 1px solid #d0d5d8;
  bottom: 0;
  display: flex;
  flex-direction: row;
  height: 44px;
  left: 0;
  position: fixed;
  right: 0;
  top: auto;
`

const NavigationLink = styled(NavLink)`
  align-items: center;
  color: #262626;
  display: flex;
  flex-grow: 1;
  height: 100%;
  justify-content: center;

  &.active {
    color: #0d7c8d;
  }
`

const BottomNavigation: React.FunctionComponent = () => (
  <NavigationWrapper>
    <Navigation>
      <NavigationLink to="/search">
        <IoIosSearch size="2.1em" />
      </NavigationLink>
      <NavigationLink to="/collection">
        <MdLibraryMusic size="1.9em" />
      </NavigationLink>
      <NavigationLink to="/user">
        <MdPersonOutline size="2.1em" />
      </NavigationLink>
    </Navigation>
  </NavigationWrapper>
)

export default BottomNavigation
