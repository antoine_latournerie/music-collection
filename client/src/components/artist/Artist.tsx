import React from 'react'
import styled from 'styled-components'

import { getImageUrl } from '../../utils/format'
import SearchResults from '../search/SearchResults'
import ParallaxImageContainer from '../ui/ParallaxImageContainer'

const CircleImage = styled.div<{ url: string }>`
  background: url('${props => props.url}') no-repeat center;
  background-size: cover;
  border: 1px solid #eaeff2;
  border-radius: 50%;
  display: flex;
  height: 190px;
  margin: 10px auto;
  width: 190px;

  @media only screen and (max-width: 420px) {
    display: none;
  }
`

const ParallaxContent = styled.div`
  @media only screen and (max-width: 420px) {
    background-color: #f4f5f7;
    padding-top: 0.9em;
  }
`

const FullWidthImageContainer = styled.div<{ url: string }>`
  background: url('${({ url }) => url}') no-repeat center;
  background-size: cover;
  display: none;
  height: 95vw;
  left: 0;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: -1;

  @media only screen and (max-width: 420px) {
    display: flex;
  }
`

const FullWidthImageHover = styled.div`
  border-bottom: 1px solid #eaeff2;
  box-shadow: inset 0 -60em 35vh -58em rgba(0, 0, 0, 0.45);
  display: none;
  height: 95vw;
  width: 100%;

  @media only screen and (max-width: 420px) {
    display: flex;
  }
`

const TitleInsidePicture = styled.h1`
  align-self: flex-end;
  color: #f4f5f7;
  display: none;
  margin: 2.8% 5.7%;

  @media only screen and (max-width: 420px) {
    display: block;
  }
`

const TitleOutsidePicture = styled.h1`
  text-align: center;
  @media only screen and (max-width: 420px) {
    display: none;
  }
`

interface Props {
  artist: SpotifyApi.ArtistObjectFull
}

const Artist: React.FunctionComponent<Props> = ({ artist }) => (
  <div>
    <CircleImage url={getImageUrl(artist.images, 400)} />
    <ParallaxImageContainer>
      <FullWidthImageContainer
        id="image"
        url={getImageUrl(artist.images, 800) || ''}
      />
    </ParallaxImageContainer>
    <FullWidthImageHover>
      <TitleInsidePicture>{artist.name}</TitleInsidePicture>
    </FullWidthImageHover>
    <TitleOutsidePicture>{artist.name}</TitleOutsidePicture>
    <ParallaxContent>
      <SearchResults albums={artist.albums} />
    </ParallaxContent>
  </div>
)

export default Artist
