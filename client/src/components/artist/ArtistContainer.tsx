import React from 'react'
import { RouteComponentProps, withRouter } from 'react-router'

import { getArtist } from '../../utils/api'
import ErrorMessage from '../ui/ErrorMessage'
import Loading from '../ui/Loading'
import Artist from './Artist'

type Props = RouteComponentProps<{ id: string }>

interface State {
  artist: SpotifyApi.ArtistObjectFull | null
  error: boolean
  loading: boolean
}

class ArtistContainer extends React.Component<Props, State> {
  readonly state: Readonly<State> = {
    artist: null,
    error: false,
    loading: false,
  }

  componentDidMount() {
    const { match } = this.props
    this.fetchArtist(match.params.id)
  }

  componentDidUpdate(prevProps: Props) {
    const { match } = this.props
    if (match.params.id !== prevProps.match.params.id) {
      this.fetchArtist(match.params.id)
    }
  }

  private async fetchArtist(id: string) {
    this.setState({ artist: null, loading: true, error: false })
    try {
      const artist = await getArtist(id)
      this.setState({ artist, loading: false })
    } catch (e) {
      this.setState({ loading: false, error: true })
    }
  }

  render() {
    const { artist, loading, error } = this.state
    if (loading) {
      return <Loading />
    }
    if (error) {
      return <ErrorMessage />
    }
    return artist && <Artist artist={artist} />
  }
}

export default withRouter(ArtistContainer)
