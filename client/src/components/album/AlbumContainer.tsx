import React from 'react'
import { RouteComponentProps, withRouter } from 'react-router'

import { getAlbum } from '../../utils/api'
import ErrorMessage from '../ui/ErrorMessage'
import Loading from '../ui/Loading'
import Album from './Album'

type Props = RouteComponentProps<{ id: string }>

interface State {
  album: SpotifyApi.AlbumObjectFull | null
  error: boolean
  loading: boolean
}

class AlbumContainer extends React.Component<Props, State> {
  readonly state: Readonly<State> = {
    album: null,
    error: false,
    loading: false,
  }

  componentDidMount() {
    const { match } = this.props
    this.fetchAlbum(match.params.id)
  }

  componentDidUpdate(prevProps: Props) {
    const { match } = this.props
    if (match.params.id !== prevProps.match.params.id) {
      this.fetchAlbum(match.params.id)
    }
  }

  private async fetchAlbum(id: string) {
    this.setState({ album: null, loading: true, error: false })
    try {
      const album = await getAlbum(id)
      this.setState({ album, loading: false })
    } catch (e) {
      this.setState({ loading: false, error: true })
    }
  }

  render() {
    const { album, loading, error } = this.state
    if (loading) {
      return <Loading />
    }
    if (error) {
      return <ErrorMessage />
    }
    return album && <Album album={album} />
  }
}

export default withRouter(AlbumContainer)
