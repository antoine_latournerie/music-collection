import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'

import { addToCollection, removeFromCollection } from '../../store/collection/actions'
import { isAlbumInCollection } from '../../store/collection/selectors'
import StoreState from '../../store/state'
import CollectionButton from './CollectionButton'

interface Props {
  album: SpotifyApi.AlbumObjectFull
}

const mapStateToProps = (state: StoreState, { album }: Props) => ({
  isInCollection: isAlbumInCollection(state, album.id),
})

const mapDispatchToProps = (dispatch: Dispatch, { album }: Props) => bindActionCreators({
  addToCollection: (e: React.MouseEvent) => {
    e.preventDefault()
    return addToCollection(album.id)
  },
  removeFromCollection: (e: React.MouseEvent) => {
    e.preventDefault()
    return removeFromCollection(album.id)
  },
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(CollectionButton)
