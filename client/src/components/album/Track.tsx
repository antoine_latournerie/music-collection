import classNames from 'classnames'
import React from 'react'
import { MdPlayArrow } from 'react-icons/md'
import styled from 'styled-components'

import { formatTimeMillis } from '../../utils/format'
import Button from '../ui/Button'

const TransparentButton = styled(Button)`
  height: 3.4em;
  padding: 0;
  position: relative;
  width: 2.6em;

  &:focus[data-focus-method=mouse] {
    outline: 0;
  }
`

/* stylelint-disable block-no-empty */
const TrackNumber = styled.span``

const PlayIcon = styled(MdPlayArrow)`
  bottom: 0;
  left: 0;
  margin: auto;
  opacity: 0;
  position: absolute;
  right: 0;
  top: 0;
`

const TrackRow = styled.tr<{ hasPreview: boolean }>`
  border-bottom: 1px solid #e0e0e0;
  border-top: 1px solid #e0e0e0;
  cursor: default;
  font-size: 0.85em;
  height: 3.6em;

  &.has-preview {
    cursor: pointer;
  }

  &.is-playing {
    background-color: #daf0f6;
  }

  &:hover {
    background-color: #eee;
  }

  &:hover,
  &.is-playing {
    border-bottom: 1px solid transparent;
    border-collapse: separate;
    border-radius: 6px;
    border-top: 1px double transparent;
  }

  &:hover td,
  &.is-playing td {
    color: #0d7c8d;
  }

  &.is-playing td:first-child {
    border-radius: 6px 0 0 6px;
    box-shadow: -8px 0 0 0 #daf0f6;
  }

  &:hover td:first-child {
    border-radius: 6px 0 0 6px;
    box-shadow: -8px 0 0 0 #eee;
  }

  &.is-playing td:last-child {
    border-radius: 0 6px 6px 0;
    box-shadow: 8px 0 0 0 #daf0f6;
  }

  &:hover td:last-child {
    border-radius: 0 6px 6px 0;
    box-shadow: 8px 0 0 0 #eee;
  }

  &:hover ${TransparentButton} ${TrackNumber},
  &.is-playing ${TrackNumber} {
    opacity: 0;
  }

  &:hover ${PlayIcon},
  &.is-playing ${PlayIcon} {
    opacity: 1;
  }
`

const TrackNumberColumn = styled.td`
  color: #8e8e8e;
  text-align: center;
  width: 2.8em;
`

const TrackNameColumn = styled.td`
  width: auto;
`

const TrackDurationColumn = styled.td`
  color: #8e8e8e;
  padding-left: 1em;
  text-align: right;
  width: 4em;
`

interface Props {
  track: SpotifyApi.TrackObjectSimplified,
  isPlaying: boolean,
  playMusic: () => void,
}

const Track: React.FunctionComponent<Props> = ({ track, isPlaying, playMusic }) => (
  <TrackRow
    onClick={track.preview_url ? playMusic : undefined}
    className={classNames({ 'is-playing': isPlaying }, { 'has-preview': track.preview_url })}
    hasPreview={!!track.preview_url}
  >
    <TrackNumberColumn>
      {track.preview_url
        ? (
          <TransparentButton>
            <TrackNumber>{track.track_number}</TrackNumber>
            <PlayIcon size="1.7em" />
          </TransparentButton>
        )
        : <TrackNumber>{track.track_number}</TrackNumber>
      }
    </TrackNumberColumn>
    <TrackNameColumn>{track.name}</TrackNameColumn>
    <TrackDurationColumn>{formatTimeMillis(track.duration_ms)}</TrackDurationColumn>
  </TrackRow>
)

export default Track
