import React, { Fragment } from 'react'

import TrackList from './TrackList'

interface Props {
  tracks: SpotifyApi.PagingObject<SpotifyApi.TrackObjectSimplified>,
}

interface State {
  trackPlaying: number | null,
}

export default class TrackListWithPlayer extends React.Component<Props, State> {
  readonly state: Readonly<State> = {
    trackPlaying: null,
  }

  mediaPlayer = React.createRef<HTMLAudioElement>()

  playMusic = (track: SpotifyApi.TrackObjectSimplified) => {
    const artists = track.artists.map(artist => artist.name).join(', ')

    this.setState({ trackPlaying: track.track_number })
    const { trackPlaying } = this.state
    if (this.mediaPlayer.current) {
      if (trackPlaying === track.track_number) {
        if (this.mediaPlayer.current.paused) {
          this.mediaPlayer.current.play()
        } else {
          this.mediaPlayer.current.pause()
        }
      } else {
        this.mediaPlayer.current.src = track.preview_url
        this.mediaPlayer.current.title = `${artists} - ${track.name}`
        this.mediaPlayer.current.play()
      }
    }
  }

  render() {
    const { tracks } = this.props
    const { trackPlaying } = this.state
    return (
      <Fragment>
        <TrackList tracks={tracks} trackPlaying={trackPlaying} playMusic={this.playMusic} />
        <audio ref={this.mediaPlayer} />
      </Fragment>
    )
  }
}
