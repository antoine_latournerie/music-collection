import React from 'react'
import styled from 'styled-components'

import Track from './Track'

const TrackListTable = styled.table`
  border-collapse: collapse;
  margin-top: 0.5em;
  padding: 0;
  width: 100%;
`

interface Props {
  tracks: SpotifyApi.PagingObject<SpotifyApi.TrackObjectSimplified>,
  trackPlaying: number | null,
  playMusic: (track: SpotifyApi.TrackObjectSimplified) => void
}

const TrackList: React.FunctionComponent<Props> = ({ tracks, trackPlaying, playMusic }) => (
  <TrackListTable>
    <tbody>
      {tracks.items.map(track => (
        <Track
          key={track.track_number}
          track={track}
          isPlaying={trackPlaying === track.track_number}
          playMusic={() => playMusic(track)}
        />
      ))}
    </tbody>
  </TrackListTable>
)

export default TrackList
