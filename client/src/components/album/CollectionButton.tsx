import React from 'react'
import { MdCheckCircle, MdLibraryAdd } from 'react-icons/md'
import styled from 'styled-components'

import Button from '../ui/Button'

const StyledButton = styled(Button)`
  align-items: center;
  display: flex;
  font-size: 0.9em;
  justify-content: center;
  padding: 0;
  text-align: left;
`

const Icon = styled.div`
  margin-right: 0.4em;
`

interface Props {
  isInCollection: boolean
  addToCollection: (e: React.MouseEvent) => any
  removeFromCollection: (e: React.MouseEvent) => any
}

const CollectionButton: React.FunctionComponent<Props> = ({
  isInCollection,
  addToCollection,
  removeFromCollection,
}) => (isInCollection
  ? (
    <StyledButton
      type="button"
      title="Remove from collection"
      onClick={removeFromCollection}
    >
      <Icon>
        <MdCheckCircle size="1.7em" />
      </Icon>
      Remove from collection
    </StyledButton>
  )
  : (
    <StyledButton
      type="button"
      title="Add to collection"
      onClick={addToCollection}
    >
      <Icon>
        <MdLibraryAdd size="1.7em" />
      </Icon>
      Add to collection
    </StyledButton>
  ))

export default CollectionButton
