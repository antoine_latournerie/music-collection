import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { getImageUrl } from '../../utils/format'
import CollectionButtonContainer from './CollectionButtonContainer'
import TrackListWithPlayer from './TrackListWithPlayer'

const StyledAlbumSheetView = styled.section`
  margin: 1em;
`

const CoverColumn = styled.div`
  display: none;

  @media only screen and (min-width: 750px) {
    box-sizing: border-box;
    display: inline-block;
    padding-right: 2em;
    vertical-align: top;
    width: 35%;
  }
`

const InformationRow = styled.div`
  display: flex;
  flex-flow: row nowrap;

  > div {
    flex-grow: 1;
    flex-shrink: 1;
  }
`

const InformationColumn = styled.div`
  flex-basis: 48%;
`

const CoverSmallView = styled.div`
  display: inline-block;
  flex-basis: 52%;
  max-width: 250px;

  @media only screen and (min-width: 750px) {
    display: none;
  }
`

const PaddedCover = styled.div`
  padding-right: 1em;
`

const Cover = styled.img`
  border: 1px solid rgba(13, 124, 141, 0.05);
  width: 100%;
`

const AlbumInformation = styled.div`
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
  width: 100%;

  @media only screen and (min-width: 750px) {
    width: 65%;
  }
`

const CollectionName = styled.span`
  font-size: 1.05em;
  font-weight: 700;
  letter-spacing: 0.02em;
  line-height: 1.2;
  margin: 0;

  @media only screen and (min-width: 750px) {
    font-size: 1.4em;
    line-height: 1.25;
  }
`

const ArtistNameLink = styled(Link)`
  color: #0d7c8d;
  display: block;
  font-size: 1.05em;
  font-weight: 400;
  letter-spacing: 0.02em;
  line-height: 1.2;
  margin-bottom: 0.1em;
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`

const ReleaseDate = styled.span`
  color: #8e8e8e;
  display: block;
  font-size: 0.85em;
  font-weight: 400;
  line-height: 1.4;
`

const CollectionRow = styled.div`
  margin-top: 0.5em;
`

interface Props {
  album: SpotifyApi.AlbumObjectFull
}

const Album: React.FunctionComponent<Props> = ({ album }) => {
  const artworkUrl = getImageUrl(album.images, 700)
  return (
    <StyledAlbumSheetView>
      <CoverColumn>
        <Cover src={artworkUrl} alt="artwork" />
      </CoverColumn>
      <AlbumInformation>
        <InformationRow>
          <CoverSmallView>
            <PaddedCover>
              <Cover src={artworkUrl} alt="artwork" />
            </PaddedCover>
          </CoverSmallView>
          <InformationColumn>
            <div>
              <CollectionName>{album.name}</CollectionName>
              {album.artists.map(artist => (
                <ArtistNameLink to={`/artist/${artist.id}`} key={artist.id}>
                  {artist.name}
                </ArtistNameLink>
              ))}
              <ReleaseDate>{album.release_date}</ReleaseDate>
            </div>
            <CollectionRow>
              <CollectionButtonContainer album={album} />
            </CollectionRow>
          </InformationColumn>
        </InformationRow>
        <TrackListWithPlayer tracks={album.tracks} />
      </AlbumInformation>
    </StyledAlbumSheetView>
  )
}

export default Album
