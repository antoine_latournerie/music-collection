import React from 'react'

const Loading: React.FunctionComponent = () => (
  <span>Loading...</span>
)

export default Loading
