import React from 'react'

export default class ParallaxImageContainer extends React.Component<{}> {
  // eslint-disable-next-line react/sort-comp
  scrollHandler: any

  static setTransformProperty(imageElement: HTMLElement) {
    // eslint-disable-next-line no-param-reassign
    imageElement.style.transform = `translateY(-${window.pageYOffset * 0.15}px)`
  }

  static handleParallax(imageElement: HTMLElement) {
    requestAnimationFrame(ParallaxImageContainer.setTransformProperty.bind(null, imageElement))
  }

  componentDidMount() {
    const { children } = this.props as { children: React.ReactElement<any> | undefined | null }
    if (children && children.props && children.props.id) {
      const imageElement = document.getElementById(children.props.id)
      this.scrollHandler = ParallaxImageContainer.handleParallax.bind(null, imageElement)
      if (imageElement) {
        ParallaxImageContainer.handleParallax(imageElement)
        window.addEventListener('scroll', this.scrollHandler, false)
      }
    }
  }

  componentWillUnmount() {
    if (this.scrollHandler) {
      window.removeEventListener('scroll', this.scrollHandler, false)
    }
  }

  render() {
    const { children: Children }: any = this.props
    return Children
  }
}
