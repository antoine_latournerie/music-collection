import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
  background: transparent;
  border: 0;
  cursor: pointer;

  &:active {
    color: #555;
  }

  &:focus[data-focus-method=mouse] {
    outline: none;
  }
`

export default class Button extends React.Component<any> {
  button = React.createRef<HTMLButtonElement>()

  setDataFocusMethod = () => {
    if (this.button.current) {
      this.button.current.setAttribute('data-focus-method', 'mouse')
    }
  }

  removeDataFocusMethod = () => {
    if (this.button.current) {
      this.button.current.removeAttribute('data-focus-method')
    }
  }

  render() {
    return (
      <StyledButton
        {...this.props}
        onMouseDown={this.setDataFocusMethod}
        onBlur={this.removeDataFocusMethod}
        ref={this.button}
      />
    )
  }
}
