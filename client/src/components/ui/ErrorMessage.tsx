import React from 'react'

const ErrorMessage: React.FunctionComponent = () => (
  <span>An error occurred, please try again</span>
)

export default ErrorMessage
