import React from 'react'

const ComponentLoading: React.FunctionComponent = () => (
  <span>Loading...</span>
)

export default ComponentLoading
