import React from 'react'
import loadable from 'loadable-components'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom'
import styled from 'styled-components'

import store from '../store'
import isMobile from '../utils/isMobile'
import BottomNavigation from './navigation/BottomNavigation'
import ComponentLoading from './ui/ComponentLoading'

const AlbumContainer = loadable(
  () => import('./album/AlbumContainer'),
  { LoadingComponent: ComponentLoading },
)

const ArtistContainer = loadable(
  () => import('./artist/ArtistContainer'),
  { LoadingComponent: ComponentLoading },
)

const CollectionContainer = loadable(
  () => import('./collection/CollectionContainer'),
  { LoadingComponent: ComponentLoading },
)

const SearchContainer = loadable(
  () => import('./search/SearchContainer'),
  { LoadingComponent: ComponentLoading },
)

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  max-width: 50em;
  min-width: 18em;
`

const App: React.FunctionComponent = () => (
  <Provider store={store}>
    <Router>
      <AppWrapper>
        <Switch>
          <Route exact path="/search" component={SearchContainer} />
          <Route path="/artist/:id" component={ArtistContainer} />
          <Route path="/album/:id" component={AlbumContainer} />
          <Route path="/collection" component={CollectionContainer} />
          <Redirect from="" to="/search" />
        </Switch>
        {isMobile() && <BottomNavigation />}
      </AppWrapper>
    </Router>
  </Provider>
)

export default App
