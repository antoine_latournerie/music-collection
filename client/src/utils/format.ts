export function padNumber(number: number, maxLength = 2): string {
  return String(number).padStart(maxLength, '0')
}

export function formatTimeMillis(timeMillis: number): string {
  const date = new Date(timeMillis)
  return date.getUTCHours() === 0
    ? `${date.getUTCMinutes()}:${padNumber(date.getUTCSeconds())}`
    : `${date.getUTCHours()}:${padNumber(date.getUTCMinutes())}:${padNumber(date.getUTCSeconds())}`
}

export function getImageUrl(images: SpotifyApi.ImageObject[], maxSize: number): string {
  const matchingImage = images.find((image) => {
    if (image.width && image.height) {
      return image.width < maxSize || image.height < maxSize
    }
    return false
  })
  return matchingImage ? matchingImage.url : ''
}
