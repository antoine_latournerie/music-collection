export default function isMobile() {
  return /Mobile/i.test(navigator.userAgent)
}
