import axios from 'axios'

import CollectionState from '../store/collection/state'

export async function searchAlbum(
  params: SpotifyApi.SearchForItemParameterObject,
): Promise<SpotifyApi.AlbumSearchResponse> {
  const url = '/api/search'
  const defaultParams = {
    limit: 25,
  }
  const result = await axios.get<SpotifyApi.AlbumSearchResponse>(url, {
    params: { ...defaultParams, ...params },
  })
  return result.data
}

export async function getAlbum(id: string): Promise<SpotifyApi.AlbumObjectFull> {
  const url = `/api/album/${id}`
  const result = await axios.get<SpotifyApi.SingleAlbumResponse>(url)
  return result.data
}

export async function getAlbums(
  collection: CollectionState,
): Promise<SpotifyApi.AlbumObjectFull[]> {
  const url = '/api/albums'
  const result = await axios.get<SpotifyApi.MultipleAlbumsResponse>(url, {
    params: { ids: collection.join(',') },
  })
  return result.data && result.data.albums
}

export async function getArtist(id: string): Promise<SpotifyApi.ArtistObjectFull> {
  const url = `/api/artist/${id}`
  const result = await axios.get<SpotifyApi.ArtistObjectFull>(url)
  return result.data
}
