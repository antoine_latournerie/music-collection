import axios from 'axios'
import express from 'express'

import { clientId, clientSecret } from './config'

const app = express()

let spotifyToken = {}

function getSpotifyToken() {
  const url = 'https://accounts.spotify.com/api/token'
  const params = new URLSearchParams()
  params.append('grant_type', 'client_credentials')
  const headers = {
    Authorization: `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString('base64')}`,
  }
  return axios.post(url, params, { headers })
}

async function updateSpotifyToken() {
  const { data: token } = await getSpotifyToken()
  spotifyToken = token
}

async function updateSpotifyTokenHourly() {
  try {
    await updateSpotifyToken()
    setTimeout(updateSpotifyTokenHourly, 3550000)
  } catch (e) {
    setTimeout(updateSpotifyTokenHourly, 5000)
  }
}

updateSpotifyTokenHourly()

const spotifyApiUrl = 'https://api.spotify.com/v1'

app.get('/api/search', async (req, res) => {
  const url = 'https://api.spotify.com/v1/search'
  const queryParams = req.query
  const headers = {
    Authorization: `${spotifyToken.token_type} ${spotifyToken.access_token}`,
  }
  const params = {
    q: queryParams.q,
    offset: queryParams.offset,
    type: 'artist,album',
    limit: queryParams.limit,
  }
  try {
    const {
      headers: responseHeaders,
      data: searchResults,
    } = await axios.get(url, { headers, params })
    res.set({
      'Cache-Control': responseHeaders['cache-control'],
    })
    res.send(searchResults)
  } catch (e) {
    res.status(e.response.status).send(e.response.data)
  }
})

app.get('/api/artist/:id', async (req, res) => {
  const { id } = req.params
  const url = `${spotifyApiUrl}/artists/${id}`
  const headers = {
    Authorization: `${spotifyToken.token_type} ${spotifyToken.access_token}`,
  }
  try {
    const [{ headers: artistHeaders, data: artist }, { data: albums }] = await Promise.all([
      axios.get(url, { headers }),
      axios.get(`${url}/albums`, { headers }),
    ])
    res.set({
      'Cache-Control': artistHeaders['cache-control'],
    })
    res.send({
      ...artist,
      albums,
    })
  } catch (e) {
    res.status(e.response.status).send(e.response.data)
  }
})

app.get('/api/album/:id', async (req, res) => {
  const { id } = req.params
  const url = `${spotifyApiUrl}/albums/${id}`
  const headers = {
    Authorization: `${spotifyToken.token_type} ${spotifyToken.access_token}`,
  }
  try {
    const [{ headers: albumHeaders, data: album }, { data: tracks }] = await Promise.all([
      axios.get(url, { headers }),
      axios.get(`${url}/tracks`, { headers }),
    ])
    res.set({
      'Cache-Control': albumHeaders['cache-control'],
    })
    res.send({
      ...album,
      tracks,
    })
  } catch (e) {
    res.status(e.response.status).send(e.response.data)
  }
})

app.get('/api/albums', async (req, res) => {
  const { ids } = req.query
  const url = `${spotifyApiUrl}/albums`
  const headers = {
    Authorization: `${spotifyToken.token_type} ${spotifyToken.access_token}`,
  }
  const params = {
    ids,
  }
  try {
    const {
      headers: responseHeaders,
      data: albumsResult,
    } = await axios.get(url, { headers, params })
    res.set({
      'Cache-Control': responseHeaders['cache-control'],
    })
    res.send(albumsResult)
  } catch (e) {
    res.status(e.response.status).send(e.response.data)
  }
})

app.listen(5000, () => {
  console.log('App listening on port 5000')
})
